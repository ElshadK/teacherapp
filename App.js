import React, { useEffect, useState } from "react";
import { SafeAreaView, ScrollView, Text, View } from "react-native";
import { LogBox } from "react-native";
import Posts from "./src/screens/posts/Index";
import Filter from "./src/screens/filter/Index";
import PostAdd from "./src/screens/postAdd/Index";
import PostUpdate from "./src/screens/postUpdate/Index";
import Login from "./src/screens/login/Index";
import Register from "./src/screens/register/Index";
import Person from "./src/screens/person/Index";
import Toast from "react-native-toast-message";
import { MainContext } from "./src/contexts/MainContext";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

const initialState = {filter:{}};

const App = () => {
  const [state, setState] = useState(initialState);
  useEffect(() => {
    LogBox.ignoreLogs(["Animated: `useNativeDriver`"]);
  }, []);

  return (
    <MainContext.Provider value={{ state, setState }}>
      <SafeAreaView style={{ backgroundColor: "#DDDDDD", height: "100%" }}>
        <NavigationContainer>
          <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Person" component={Person} />
            <Stack.Screen name="Posts" component={Posts} />
            <Stack.Screen name="Filter" component={Filter} />
            <Stack.Screen name="PostAdd" component={PostAdd} />
            <Stack.Screen name="PostUpdate" component={PostUpdate} />
          </Stack.Navigator>
        </NavigationContainer>
        <Toast />
      </SafeAreaView>
    </MainContext.Provider>
  );
};

export default App;
