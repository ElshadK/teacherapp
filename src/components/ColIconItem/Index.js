import React from "react";
import { Col } from "react-native-easy-grid";
import { StyleSheet } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export const ColIconItem = (props) => {
  return (
    <Col style={{padding:3}} size={props.size}>
      <MaterialCommunityIcons name={props.iconName} size={20} color="#37a000" style={styles.icon}/>
      {/* <Icon name={props.iconName} type={props.iconType} style={styles.icon} /> */}
    </Col>
  );
};

const styles = StyleSheet.create({
  icon: {
  },
});
