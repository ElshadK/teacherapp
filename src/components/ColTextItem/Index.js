import React from "react";
import { Col } from "react-native-easy-grid";
import { StyleSheet, Text } from "react-native";

export const ColTextItem = (props) => {
  return (
    <Col style={{ padding: 3 }} size={props.size}>
      <Text style={styles.header}>{props.header}</Text>
      <Text style={styles.content}>{props.content}</Text>
    </Col>
  );
};

const styles = StyleSheet.create({
  header: {
    color: "#1d4354",
    fontWeight: "bold",
    //   fontSize: Constant.HEADER_TEXT_SIZE,
    fontStyle: "italic",
  },
  content: { fontSize: 14, color: "#a0a0a0",},
});
