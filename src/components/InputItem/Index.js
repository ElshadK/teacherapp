import React from "react";
import { StyleSheet, View, TextInput } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export const InputItem = ({ value, name, placeholder, onChange, icon, multiline, isSecure, type="default" }) => {
  return (
    <View style={styles.searchSection}>
      <MaterialCommunityIcons name={icon} size={20} color="#37a000" style={styles.searchIcon} />
      <TextInput style={styles.input} placeholder={placeholder} value={value} name={name} keyboardType={type} secureTextEntry={isSecure && true} multiline={multiline && false} numberOfLines={multiline && 5} underlineColorAndroid="transparent" onChangeText={(val) => onChange(name, val)} />
    </View>
  );
};

const styles = StyleSheet.create({
  searchSection: {
    // flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    elevation: 5,
    //borderColor:'#a0a0a0',
    // borderWidth:0.5,
    borderRadius: 5,
    margin: 3,
    width: "98%",
  },
  searchIcon: {
    padding: 10,
    alignSelf: "flex-start",
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: "#fff",
    color: "#424242",
    textAlignVertical: "top",
    borderRadius:5
  },
});
