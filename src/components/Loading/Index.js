import React from "react";
import { ActivityIndicator, View } from "react-native";

export default function Index() {
  return (
    <View style={{ marginVertical: "100%" }}>
      <ActivityIndicator size="large" color="green" />
    </View>
  );
}
