import React, { useState } from "react";
import { Text, View } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default function Index({ title, icon , items}) {
 // console.log(items);
   const [open, setOpen] = useState(false);
  // const [value, setValue] = useState("0");
  // const [items, setItems] = useState([
  //   {
  //     label: title,
  //     value: "0",
  //     icon: () => (
  //       <MaterialCommunityIcons name={icon} size={20} color="#37a000" />
  //     ),
  //   },
  // ]);

  return (
    <DropDownPicker
      style={{
        borderColor: "white",
        elevation: 5,
        margin: 3,
        width: "98%",
        borderRadius: 5,
      }}
      listMode="MODAL"
      modalProps={{
        animationType: "fade",
      }}
      modalTitle={title}
      modalContentContainerStyle={{
        borderColor: "red",
      }}
      modalTitleStyle={{
        fontWeight: "bold",
      }}
      showArrowIcon={true}
      showTickIcon={true}
      placeholder={title}
      open={open}
     // value={value}
      items={items}
      setOpen={setOpen}
      // setValue={setValue}
      // setItems={setItems}
    />
  );
}
