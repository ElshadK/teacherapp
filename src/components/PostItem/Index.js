import React, { useState } from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import { Col, Grid, Row } from "react-native-easy-grid";
import { ColIconItem } from "../../components/ColIconItem/Index";
import { ColTextItem } from "../../components/ColTextItem/Index";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export const PostItem = ({ post, currentUserId, deletePost, updatePost }) => {
  return (
    <View style={styles.container}>
      <Row>
        <Col size={13}>
          <Text style={styles.header}>{`${post?.user?.name} ${post?.user?.surname}`}</Text>
        </Col>
        {post?.user?.userId == currentUserId ? (
          <>
            <Col size={1.5}>
            <TouchableOpacity onPress={() => updatePost(post?.postId)}>
                <MaterialCommunityIcons name={"circle-edit-outline"} size={30} color="#11468F" />
              </TouchableOpacity>
            </Col>
            <Col size={1.5}>
              <TouchableOpacity onPress={() => deletePost(post?.postId)}>
                <MaterialCommunityIcons name={"close-circle-outline"} size={30} color="#D82148" />
              </TouchableOpacity>
            </Col>
          </>
        ) : null}
      </Row>

      <Row style={styles.row}>
        <ColIconItem size={1} iconName={"city"} />
        <ColTextItem size={8} header={"İşlədiyim rayon"} content={post?.fromRegion?.name} />

        <ColIconItem size={1} iconName={"city"} />
        <ColTextItem size={8} header={"Yerdəyişmə rayonu"} content={post?.toRegion?.name} />
      </Row>

      <Row style={styles.row}>
        <ColIconItem size={1} iconName={"school-outline"} />
        <ColTextItem size={8} header={"İşlədiyim məktəb"} content={post?.fromSchool?.name} />
        <ColIconItem size={1} iconName={"school-outline"} />
        <ColTextItem size={8} header={"Yerdəyişmə məktəbi"} content={post?.toSchool?.name} />
      </Row>

      <Row style={styles.row}>
        <ColIconItem size={1} iconName={"book-education-outline"} />
        <ColTextItem size={8} header={"İxtisas"} content={post?.subject?.name} />

        <ColIconItem size={1} iconName={"av-timer"} />
        <ColTextItem size={8} header={"Dərs saatı"} content={`${post?.lessonTime} saat`} />
      </Row>

      <Row style={styles.row}>
        <ColIconItem size={1} iconName={"phone-in-talk-outline"} />
        <ColTextItem size={8} header={"Telefon nömrəsi"} content={post?.user?.phone} />

        <ColIconItem size={1} iconName={"email-outline"} />
        <ColTextItem size={8} header={"Email adresi"} content={post?.user?.mail} />
      </Row>
      <Row style={styles.row}>
        <ColIconItem size={1} iconName={"note-text-outline"} />
        <ColTextItem size={16} header={"Qeyd"} content={post?.note} />
      </Row>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    elevation: 1,
    margin: 0,
    //width: "99%",
    // height: 260,
    //  backgroundColor: "#37a000",
    borderRadius: 3,
    padding: 10,
  },
  header: {
    fontWeight: "bold",
    fontSize: 17,
    color: "#1d4354",
    margin: 10,
  },
  row: { margin: 0 },
});
