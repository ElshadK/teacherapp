import React, { useState, useEffect } from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";

export default function Index({ tab, setTab }) {
  return (
    <View style={{ flexDirection: "row", width: "100%", paddingTop: 10, paddingBottom:10 }}>
      <TouchableOpacity style={tab == "personal" ? styles.btnTabSelect : styles.btnTab} onPress={() => setTab("personal")}>
        <Text style={tab == "personal" ? styles.txtTabSelect : styles.txtTab}>Şəxsi məlumatlarım</Text>
      </TouchableOpacity>
      <TouchableOpacity style={tab == "myPosts" ? styles.btnTabSelect : styles.btnTab} onPress={() => setTab("myPosts")}>
        <Text style={tab == "myPosts" ? styles.txtTabSelect : styles.txtTab}>Şifrənin dəyişilməsi</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  listItem: { padding: 10, backgroundColor: "white", elevation: 5 },
  listText: { fontSize: 17, color: "black" },
  btnTabSelect: {
    backgroundColor: "#37a000",
    flex: 1,
    padding: 15,
    elevation: 5,
  },
  btnTab: {
    backgroundColor: "white",
    flex: 1,
    padding: 15,
    elevation: 5,
    borderBottomColor: "#37a000",
    borderBottomWidth: 2,
  },

  txtTabSelect: {
    color: "white",
    fontSize: 15,
    textAlign: "center",
    fontWeight: "bold",
  },
  txtTab: {
    color: "black",
    fontSize: 15,
    textAlign: "center",
    fontWeight: "bold",
  },
});
