import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, Button, ScrollView, TouchableOpacity } from "react-native";
import { InputItem } from "./../../components/InputItem/Index";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import * as helperService from "./../../services/HelperService";
import * as postService from "./../../services/PostService";
import DropDownPicker from "react-native-dropdown-picker";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-toast-message";
import { Col, Row } from "react-native-easy-grid";
import { useMainContext } from "./../../contexts/MainContext";
import CheckBox from "@react-native-community/checkbox";
import * as Storage from "./../../utils/Storage";

export default function Index() {
  const navigation = useNavigation();

  const { state, setState } = useMainContext();
  const [subjects, setSubjects] = useState([]);
  const [regions, setRegions] = useState([]);
  const [fromSchools, setFromSchools] = useState([]);
  const [toSchools, setToSchools] = useState([]);
  const [loading, setLoading] = useState(false);
  const [openFromRegion, setOpenFromRegion] = useState(false);
  const [openToRegion, setOpenToRegion] = useState(false);
  const [openFromSchool, setOpenFromSchool] = useState(false);
  const [openToSchool, setOpenToSchool] = useState(false);
  const [openSubject, setOpenSubject] = useState(false);
  const [isMyPost, setIsMyPost] = useState(false);

  useEffect(() => {
    getSubjects();
    getRegions();
  }, []);

  const getSubjects = async () => {
    let result = await helperService.getSubjects();
    if (result?.success) {
      result?.data.forEach(function (element) {
        element.icon = () => <MaterialCommunityIcons name={"book-education-outline"} size={20} color="#37a000" />;
      });
      setSubjects(result?.data);
    }
  };

  const getRegions = async () => {
    let result = await helperService.getRegions();
    if (result?.success) {
      result?.data.forEach(function (element) {
        element.icon = () => <MaterialCommunityIcons name={"city"} size={20} color="#37a000" />;
      });
      setRegions(result?.data);
    }
  };

  const getFromSchools = async (regionId) => {
    let result = await helperService.getSchools(regionId);
    if (result?.success) {
      result?.data.forEach(function (element) {
        element.icon = () => <MaterialCommunityIcons name={"school-outline"} size={20} color="#37a000" />;
      });
      setFromSchools(result?.data);
    }
  };

  const getToSchools = async (regionId) => {
    let result = await helperService.getSchools(regionId);
    if (result?.success) {
      result?.data.forEach(function (element) {
        element.icon = () => <MaterialCommunityIcons name={"school-outline"} size={20} color="#37a000" />;
      });
      setToSchools(result?.data);
    }
  };

  const filterPosts = async () => {
    // console.log(state);
    if (isMyPost) {
      let currentUserId = await Storage.getItem("@userId");
      setState({ ...state, filter: { ...state?.filter, userId: currentUserId } });
    }; //else setState({ ...state, filter: { ...state?.filter, userId: 0 } });

    navigation.push("Posts");

    // let result = await postService.addPost(post);
    // if (result?.success) {
    //   Toast.show({ type: "success", text1: "Uğurlu əməliyyat", text2: "Post əlavə edildi" });
    //   navigation.navigate("Posts");
    // } else {
    //   Toast.show({ type: "error", text1: "Uğursuz əməliyyat", text2: "Zəhmət olmasa məlumatları düzgün daxil edin" });
    // }
  };

  const onChange = (name, val) => {
    setState({ ...state, filter: { ...state?.filter, [name]: val } });
  };

  return (
    <>
      <ScrollView>
        <DropDownPicker
          style={styles.picker}
          listMode="MODAL"
          modalProps={{ animationType: "fade" }}
          modalTitle={"İşlədiyi rayon"}
          modalTitleStyle={{ fontWeight: "bold" }}
          placeholder={"İşlədiyi rayon"}
          open={openFromRegion}
          value={state?.filter?.fromRegionId}
          items={regions}
          setOpen={setOpenFromRegion}
          onSelectItem={(item) => {
            setState({ ...state, filter: { ...state?.filter, fromRegionId: item.value } });
            getFromSchools(item.value);
          }}
        />

        <DropDownPicker
          style={styles.picker}
          listMode="MODAL"
          modalProps={{ animationType: "fade" }}
          modalTitle={"İşlədiyi məktəb"}
          modalTitleStyle={{ fontWeight: "bold" }}
          placeholder={"İşlədiyi məktəb"}
          open={openFromSchool}
          value={state?.filter?.fromSchoolId}
          items={fromSchools}
          setOpen={setOpenFromSchool}
          onSelectItem={(item) => {
            setState({ ...state, filter: { ...state?.filter, fromSchoolId: item.value } });
          }}
        />

        <DropDownPicker
          style={styles.picker}
          listMode="MODAL"
          modalProps={{
            animationType: "fade",
          }}
          modalTitle={"Yerdəyişmə rayon"}
          modalTitleStyle={{ fontWeight: "bold" }}
          placeholder={"Yerdəyişmə rayon"}
          open={openToRegion}
          value={state?.filter?.toRegionId}
          items={regions}
          setOpen={setOpenToRegion}
          onSelectItem={(item) => {
            setState({ ...state, filter: { ...state?.filter, toRegionId: item.value } });
            getToSchools(item.value);
          }}
        />

        <DropDownPicker
          style={styles.picker}
          listMode="MODAL"
          modalProps={{ animationType: "fade" }}
          modalTitle={"Yerdəyişmə məktəb"}
          modalTitleStyle={{ fontWeight: "bold" }}
          placeholder={"Yerdəyişmə məktəb"}
          open={openToSchool}
          value={state?.filter?.toSchoolId}
          items={toSchools}
          setOpen={setOpenToSchool}
          onSelectItem={(item) => {
            setState({ ...state, filter: { ...state?.filter, toSchoolId: item.value } });
          }}
        />

        <DropDownPicker
          style={styles.picker}
          listMode="MODAL"
          modalProps={{ animationType: "fade" }}
          modalTitle={"İxtisas"}
          modalTitleStyle={{ fontWeight: "bold" }}
          placeholder={"İxtisas"}
          open={openSubject}
          value={state?.filter?.subjectId}
          items={subjects}
          setOpen={setOpenSubject}
          onSelectItem={(item) => {
            setState({ ...state, filter: { ...state?.filter, subjectId: item.value } });
          }}
        />

        <Row>
          <Col>
            <InputItem icon={"av-timer"} placeholder={"Dərs saatı (min)"} value={state?.filter?.lessonTimeMin} name={"lessonTimeMin"} type={"numeric"} onChange={onChange} />
          </Col>
          <Col>
            <InputItem icon={"av-timer"} placeholder={"Dərs saatı (max)"} value={state?.filter?.lessonTimeMax} name={"lessonTimeMax"} type={"numeric"} onChange={onChange} />
          </Col>
        </Row>

        <View style={styles.checkboxContainer}>
          <CheckBox value={isMyPost} onValueChange={(val) => setIsMyPost(val)} style={styles.checkbox} />
          <Text style={styles.label}>Mənim postlarım</Text>
        </View>
      </ScrollView>

      <Row style={{ alignItems: "flex-end" }}>
        <Col>
          <TouchableOpacity style={styles.btnShare} onPress={filterPosts}>
            <Text style={{ color: "white", fontSize: 18, fontWeight: "bold" }}>Filtirlə</Text>
          </TouchableOpacity>
        </Col>
        <Col>
          <TouchableOpacity style={styles.btnBack} onPress={() => navigation.navigate("Posts")}>
            <Text style={{ color: "white", fontSize: 18, fontWeight: "bold" }}>Geri qayıt</Text>
          </TouchableOpacity>
        </Col>
      </Row>
      <TouchableOpacity
        style={styles.btnClear}
        onPress={() => {
          setState({ ...state, filter: {} });
          setIsMyPost(false);
        }}
      >
        <Text style={{ color: "black", fontSize: 18, fontWeight: "bold" }}>Təmizlə</Text>
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 3,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  picker: {
    borderColor: "white",
    elevation: 5,
    margin: 3,
    width: "98%",
    borderRadius: 5,
  },
  btnShare: {
    height: 60,
    marginTop: 10,
    backgroundColor: "#37a000",
    justifyContent: "center",
    alignItems: "center",
  },
  btnBack: {
    height: 60,
    marginTop: 10,
    backgroundColor: "#D82148",
    justifyContent: "center",
    alignItems: "center",
  },
  btnClear: {
    height: 60,
    marginTop: 10,
    marginBottom: 5,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
});
