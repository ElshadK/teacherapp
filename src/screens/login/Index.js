import React, { useState, useEffect } from "react";
import { Text, StyleSheet, View, ActivityIndicator, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { InputItem } from "./../../components/InputItem/Index";
import * as userService from "./../../services/UserService";
import * as Storage from "./../../utils/Storage";
import Loading from "./../../components/Loading/Index";
import Toast from "react-native-toast-message";

export default function Index() {
  const navigation = useNavigation();
  //   const { state, setState } = useMainContext();
  const [loading, setLoading] = useState(false);

  const [login, setLogin] = useState({
    mail: "",
    password: "",
  });

  const onChange = (name, val) => {
    setLogin({ ...login, [name]: val });
  };

  const loginUser = async () => {
    setLoading(true);
    let result = await userService.loginUser(login);
    if (result?.success) {
      Toast.show({ type: "success", text1: "Uğurlu əməliyyat", text2: "Sistemə daxil oldunuz" });
      await Storage.setItem("@token", result?.data?.token);
      await Storage.setItem("@userId", result?.data?.user?.userId.toString());
      navigation.navigate("Posts");
    } else {
      Toast.show({ type: "error", text1: "Uğursuz əməliyyat", text2: "Zəhmət olmasa məlumatları düzgün daxil edin" });
    }
    setLoading(false);
  };

  return (
    <View>
      <View style={{ marginLeft: 20, marginRight: 20, justifyContent: "center", height: "100%" }}>
        <Text style={styles.headText}>Müəllimlərin qarşılıqlı yerdəyişməsi</Text>
        <InputItem icon={"email-outline"} placeholder={"Email adresi"} value={login?.mail} name={"mail"} onChange={onChange} />
        <InputItem icon={"lock-outline"} placeholder={"Şifrə"} value={login?.password} name={"password"} isSecure={true} onChange={onChange} />
        <TouchableOpacity style={styles.login} onPress={loginUser}>
          {loading ? <ActivityIndicator size="large" color="white" /> : <Text style={{ color: "white", fontSize: 18, fontWeight: "bold" }}>Daxil ol</Text>}
        </TouchableOpacity>
        <View style={{ flexDirection: "row", justifyContent: "center", padding: 20 }}>
          <Text style={{ color: "#1d4354" }}>Hesabınız yoxdur?</Text>
          <TouchableOpacity onPress={() => navigation.navigate("Register")}>
            <Text style={{ color: "#37a000" }}> Qeydiyyatdan keçin</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  login: {
    height: 60,
    marginTop: 10,
    margin: 3,
    backgroundColor: "#37a000",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
  },
  loginText: { fontSize: 20, alignSelf: "center", color: "white" },
  headText: {
    justifyContent: "center",
    textAlign: "center",
    paddingBottom: 20,
    fontSize: 30,
    fontWeight: "bold",
  },
});
