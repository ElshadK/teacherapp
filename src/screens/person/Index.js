import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, Button, ScrollView, TouchableOpacity } from "react-native";
import { InputItem } from "./../../components/InputItem/Index";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import PickerItem from "./../../components/PickerItem/Index";
import CheckBox from "@react-native-community/checkbox";
import * as helperService from "./../../services/HelperService";
import * as userService from "./../../services/UserService";
import DropDownPicker from "react-native-dropdown-picker";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-toast-message";
import { Col, Row } from "react-native-easy-grid";
import { useMainContext } from "./../../contexts/MainContext";
import Tab from "./../../components/Tab/Index";
import * as Storage from "./../../utils/Storage";

export default function Index() {
  const navigation = useNavigation();
  const { state, setState } = useMainContext();

  const [user, setUser] = useState([]);
  const [tab, setTab] = useState("personal");
  const [password, setPassword] = useState();

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    let result = await userService.getUser();
    if (result?.success) {
      setUser(result?.data);
    } else {
      Toast.show({ type: "error", text1: "Uğursuz əməliyyat", text2: "Zəhmət olmasa yenidən cəhd edin" });
    }
  };

  const updateUser = async () => {
    let result = await userService.updateUser(user);
    if (result?.success) {
      setUser(result?.data);
      Toast.show({ type: "success", text1: "Uğurlu əməliyyat", text2: "Məlumatlar yeniləndi" });
    } else {
      Toast.show({ type: "error", text1: "Uğursuz əməliyyat", text2: "Zəhmət olmasa məlumatları düzgün daxil edin" });
    }
  };

  const changePassword = async () => {
    if (password?.newPass == password?.newPassRepeate) {
      let result = await userService.getUser();
      if (result?.success) {
        setPassword({});
        Toast.show({ type: "success", text1: "Uğurlu əməliyyat", text2: "Şifrə yeniləndi" });
      } else {
        Toast.show({ type: "error", text1: "Uğursuz əməliyyat", text2: "Zəhmət olmasa məlumatları düzgün daxil edin" });
      }
    } else {
      Toast.show({ type: "error", text1: "Uğursuz əməliyyat", text2: "Şifrə və təkrar şifrə uyğun deyil" });
    }
  };

  const onChange = (name, val) => {
    setUser({ ...user, [name]: val });
  };

  const onChangePassword = (name, val) => {
    setPassword({ ...password, [name]: val });
  };

  return (
    <>
      <ScrollView>
        <View style={{ alignItems: "center", margin: 20 }}>
          <View style={{ padding: 20, backgroundColor: "white", elevation: 2, margin: 5, width: 100, height: 100, borderRadius: 50, justifyContent: "center", alignItems: "center" }}>
            <MaterialCommunityIcons name={"emoticon-happy-outline"} size={50} color="#1B1A17" />
          </View>
          <View>
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>{`${user?.surname} ${user?.name}`}</Text>
          </View>
        </View>

        <Tab tab={tab} setTab={setTab} />

        {tab == "personal" ? (
          <>
            <InputItem icon={"account-outline"} placeholder={"Ad"} value={user?.name} name={"name"} onChange={onChange} />
            <InputItem icon={"account-outline"} placeholder={"Soyad"} value={user?.surname} name={"surname"} onChange={onChange} />
            <InputItem icon={"email-outline"} placeholder={"Email adresi"} value={user?.mail} name={"mail"} type={"email-address"} onChange={onChange} />
            <InputItem icon={"phone-outline"} placeholder={"Telefon nömrəsi"} value={user?.phone} name={"phone"} type={"numeric"} onChange={onChange} />
          </>
        ) : (
          <>
            <InputItem icon={"lock-outline"} placeholder={"Yeni şifrə"} value={password?.newPass} name={"newPass"} isSecure={true} onChange={onChangePassword} />
            <InputItem icon={"lock-outline"} placeholder={"Yeni şifrə (təkrar)"} value={password?.newPassRepeate} name={"newPassRepeate"} isSecure={true} onChange={onChangePassword} />
          </>
        )}
      </ScrollView>

      {tab === "personal" ? (
        <Row style={{ alignItems: "flex-end" }}>
          <Col>
            <TouchableOpacity style={styles.btnShare} onPress={updateUser}>
              <Text style={{ color: "white", fontSize: 17, fontWeight: "bold" }}>Məlumatları yenilə</Text>
            </TouchableOpacity>
          </Col>
          <Col>
            <TouchableOpacity style={styles.btnBack} onPress={() => navigation.navigate("Posts")}>
              <Text style={{ color: "white", fontSize: 17, fontWeight: "bold" }}>Geri qayıt</Text>
            </TouchableOpacity>
          </Col>
        </Row>
      ) : (
        <Row style={{ alignItems: "flex-end" }}>
          <Col>
            <TouchableOpacity style={styles.btnShare} onPress={changePassword}>
              <Text style={{ color: "white", fontSize: 17, fontWeight: "bold" }}>Şifrəni dəyiş</Text>
            </TouchableOpacity>
          </Col>
          <Col>
            <TouchableOpacity style={styles.btnBack} onPress={() => navigation.navigate("Posts")}>
              <Text style={{ color: "white", fontSize: 17, fontWeight: "bold" }}>Geri qayıt</Text>
            </TouchableOpacity>
          </Col>
        </Row>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 3,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  picker: {
    borderColor: "white",
    elevation: 5,
    margin: 3,
    width: "98%",
    borderRadius: 5,
  },
  btnShare: {
    height: 60,
    marginTop: 10,
    backgroundColor: "#37a000",
    justifyContent: "center",
    alignItems: "center",
  },
  btnBack: {
    height: 60,
    marginTop: 10,
    backgroundColor: "#D82148",
    justifyContent: "center",
    alignItems: "center",
  },
  btnPass: {
    height: "100%",
    margin: 3,
    borderRadius: 5,
    backgroundColor: "#37a000",
    justifyContent: "center",
    alignItems: "center",
  },
});
