import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, Button, ScrollView, TouchableOpacity } from "react-native";
import { InputItem } from "./../../components/InputItem/Index";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import PickerItem from "./../../components/PickerItem/Index";
import CheckBox from "@react-native-community/checkbox";
import * as helperService from "./../../services/HelperService";
import * as postService from "./../../services/PostService";
import DropDownPicker from "react-native-dropdown-picker";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-toast-message";
import { Col, Row } from "react-native-easy-grid";
import { useMainContext } from "./../../contexts/MainContext";

export default function Index() {
  const navigation = useNavigation();
  const { state, setState } = useMainContext();

  const [subjects, setSubjects] = useState([]);
  const [regions, setRegions] = useState([]);
  const [fromSchools, setFromSchools] = useState([]);
  const [toSchools, setToSchools] = useState([]);

  const [loading, setLoading] = useState(false);
  const [openFromRegion, setOpenFromRegion] = useState(false);
  const [openToRegion, setOpenToRegion] = useState(false);
  const [openFromSchool, setOpenFromSchool] = useState(false);
  const [openToSchool, setOpenToSchool] = useState(false);
  const [openSubject, setOpenSubject] = useState(false);

  const [post, setPost] = useState();

  useEffect(() => {
    getSubjects();
    getRegions();
  }, []);

  const getSubjects = async () => {
    let result = await helperService.getSubjects();
    if (result?.success) {
      result?.data.forEach(function (element) {
        element.icon = () => <MaterialCommunityIcons name={"book-education-outline"} size={20} color="#37a000" />;
      });
      setSubjects(result?.data);
    }
  };

  const getRegions = async () => {
    let result = await helperService.getRegions();
    if (result?.success) {
      result?.data.forEach(function (element) {
        element.icon = () => <MaterialCommunityIcons name={"city"} size={20} color="#37a000" />;
      });
      setRegions(result?.data);
    }
  };

  const getFromSchools = async (regionId) => {
    let result = await helperService.getSchools(regionId);
    if (result?.success) {
      result?.data.forEach(function (element) {
        element.icon = () => <MaterialCommunityIcons name={"school-outline"} size={20} color="#37a000" />;
      });
      setFromSchools(result?.data);
    }
  };

  const getToSchools = async (regionId) => {
    let result = await helperService.getSchools(regionId);
    if (result?.success) {
      result?.data.forEach(function (element) {
        element.icon = () => <MaterialCommunityIcons name={"school-outline"} size={20} color="#37a000" />;
      });
      setToSchools(result?.data);
    }
  };

  const addPost = async () => {
    let result = await postService.addPost(post);
    if (result?.success) {
      Toast.show({ type: "success", text1: "Uğurlu əməliyyat", text2: "Post əlavə edildi" });
      navigation.navigate("Posts");
    } else {
      Toast.show({ type: "error", text1: "Uğursuz əməliyyat", text2: "Zəhmət olmasa məlumatları düzgün daxil edin" });
    }
  };

  const onChange = (name, val) => {
    setPost({ ...post, [name]: val });
  };

  return (
    <>
      <ScrollView>
        <DropDownPicker
          style={styles.picker}
          listMode="MODAL"
          modalProps={{
            animationType: "fade",
          }}
          modalTitle={"İşlədiyim rayon"}
          modalTitleStyle={{
            fontWeight: "bold",
          }}
          placeholder={"İşlədiyim rayon"}
          open={openFromRegion}
          value={post?.fromRegionId}
          items={regions}
          setOpen={setOpenFromRegion}
          onSelectItem={(item) => {
            setPost({ ...post, fromRegionId: item.value });
            getFromSchools(item.value);
          }}
        />

        <DropDownPicker
          style={styles.picker}
          listMode="MODAL"
          modalProps={{
            animationType: "fade",
          }}
          modalTitle={"İşlədiyim məktəb"}
          modalTitleStyle={{
            fontWeight: "bold",
          }}
          placeholder={"İşlədiyim məktəb"}
          open={openFromSchool}
          value={post?.fromSchoolId}
          items={fromSchools}
          setOpen={setOpenFromSchool}
          onSelectItem={(item) => {
            setPost({ ...post, fromSchoolId: item.value });
          }}
        />

        <DropDownPicker
          style={styles.picker}
          listMode="MODAL"
          modalProps={{
            animationType: "fade",
          }}
          modalTitle={"Yerdəyişmə rayon"}
          modalTitleStyle={{
            fontWeight: "bold",
          }}
          placeholder={"Yerdəyişmə rayon"}
          open={openToRegion}
          value={post?.toRegionId}
          items={regions}
          setOpen={setOpenToRegion}
          onSelectItem={(item) => {
            setPost({ ...post, toRegionId: item.value });
            getToSchools(item.value);
          }}
        />

        <DropDownPicker
          style={styles.picker}
          listMode="MODAL"
          modalProps={{
            animationType: "fade",
          }}
          modalTitle={"Yerdəyişmə məktəb"}
          modalTitleStyle={{
            fontWeight: "bold",
          }}
          placeholder={"Yerdəyişmə məktəb"}
          open={openToSchool}
          value={post?.toSchoolId}
          items={toSchools}
          setOpen={setOpenToSchool}
          onSelectItem={(item) => {
            setPost({ ...post, toSchoolId: item.value });
          }}
        />

        <DropDownPicker
          style={styles.picker}
          listMode="MODAL"
          modalProps={{
            animationType: "fade",
          }}
          modalTitle={"İxtisas"}
          modalTitleStyle={{
            fontWeight: "bold",
          }}
          placeholder={"İxtisas"}
          open={openSubject}
          value={post?.subjectId}
          items={subjects}
          setOpen={setOpenSubject}
          onSelectItem={(item) => {
            setPost({ ...post, subjectId: item.value });
          }}
        />

        <InputItem icon={"av-timer"} placeholder={"Dərs saatı"} value={post?.lessonTime} type={"numeric"} name={"lessonTime"} onChange={onChange} />
        <InputItem icon={"note-text-outline"} placeholder={"Qeyd"} multiline={true} value={post?.note} name={"note"} onChange={onChange} />

        {/* <View style={styles.checkboxContainer}>
          <CheckBox
            value={true}
            // onValueChange={setSelection}
            style={styles.checkbox}
          />
          <Text style={styles.label}>Telefon görünsün</Text>
        </View>
        <View style={styles.checkboxContainer}>
          <CheckBox
            value={true}
            // onValueChange={setSelection}
            style={styles.checkbox}
          />
          <Text style={styles.label}>Email görünsun</Text>
        </View> */}
      </ScrollView>

      <Row style={{ alignItems: "flex-end" }}>
        <Col>
          <TouchableOpacity style={styles.btnShare} onPress={addPost}>
            <Text style={{ color: "white", fontSize: 18, fontWeight: "bold" }}>Postu paylaş</Text>
          </TouchableOpacity>
        </Col>
        <Col>
          <TouchableOpacity style={styles.btnBack} onPress={() => navigation.navigate("Posts")}>
            <Text style={{ color: "white", fontSize: 18, fontWeight: "bold" }}>Geri qayıt</Text>
          </TouchableOpacity>
        </Col>
      </Row>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 3,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  picker: {
    borderColor: "white",
    elevation: 5,
    margin: 3,
    width: "98%",
    borderRadius: 5,
  },
  btnShare: {
    height: 60,
    marginTop: 10,
    backgroundColor: "#37a000",
    justifyContent: "center",
    alignItems: "center",
  },
  btnBack: {
    height: 60,
    marginTop: 10,
    backgroundColor: "#D82148",
    justifyContent: "center",
    alignItems: "center",
  },
});
