import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, ActivityIndicator, Alert, FlatList, TouchableOpacity } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import { Col, Grid, Row } from "react-native-easy-grid";
import { ColIconItem } from "../../components/ColIconItem/Index";
import { ColTextItem } from "../../components/ColTextItem/Index";
import ActionButton from "react-native-action-button";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { PostItem } from "./../../components/PostItem/Index";
import { useNavigation } from "@react-navigation/native";
import * as postService from "./../../services/PostService";
import Loading from "./../../components/Loading/Index";
import { useMainContext } from "./../../contexts/MainContext";
import * as Storage from "./../../utils/Storage";

export default function Index() {
  const navigation = useNavigation();
  const { state, setState } = useMainContext();

  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);

  const [pageNumber, setPageNumber] = useState(1);
  const [currentUserId, setCurrentUserId] = useState(0);

  useEffect(() => {
    navigation.addListener("focus", () => {
      setPageNumber(1);
      setPosts([]);
      GetCurrentUser();
      getPosts();
    });
  }, [navigation]);

  const GetCurrentUser = async () => {
    let userId = await Storage.getItem("@userId");
    setCurrentUserId(userId);
  };

  const getPosts = async (page) => {
    setLoading(true);
    let result = await postService.getPosts(page != null ? page : pageNumber, 10, state?.filter);
    if (result?.success) setPosts([...posts, ...result?.data]);
    setLoading(false);
  };

  const deletePostConfirm = async (postId) => {
    Alert.alert("Diqqət", "Bu postu silmək istədiyinizə əminsiniz?", [
      {
        text: "Xeyr",
        //onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      { text: "Bəli", onPress: () => deletePost(postId) },
    ]);
  };

  const updatePost = async (postId) => {
    navigation.navigate("PostUpdate", { postId });
  };

  const deletePost = async (postId) => {
    let result = await postService.deletePost(postId);
    if (result?.success) {
      setPageNumber(1);
      setPosts([]);
      getPosts();
    }
  };

  const exit = async () => {
    await Storage.removeItem("@userId");
    await Storage.removeItem("@token");
    navigation.navigate("Login");
  };

  const renderItem = (item) => {
    return <PostItem key={item?.item?.postId} post={item?.item} currentUserId={currentUserId} deletePost={deletePostConfirm} updatePost={updatePost} />;
  };

  const renderFooter = () => {
    return (
      //Footer View with Load More button
      <View style={styles.footer}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => {
            setPageNumber(pageNumber + 1);
            getPosts(pageNumber + 1);
          }}
          style={styles.loadMoreBtn}
        >
          <Text style={styles.btnText}>Daha çox</Text>
          {loading ? <ActivityIndicator color="white" style={{ marginLeft: 8 }} /> : null}
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <>
      {loading && posts.length == 0 ? (
        <Loading />
      ) : posts.length > 0 ? (
        <FlatList data={posts} keyExtractor={(item, index) => index.toString()} enableEmptySections={true} renderItem={renderItem} ListFooterComponent={renderFooter} />
      ) : (
        <View style={{ justifyContent: "center", height: "100%", alignSelf: "center" }}>
          <Text style={{ textAlign: "center", fontSize: 25, margin: 10 }}>Filtirə uyğun post tapılmadı</Text>
        </View>
      )}

      <ActionButton buttonColor="rgba(231,76,60,1)" size={65}>
        <ActionButton.Item buttonColor="#4D77FF" title="Şəxsi məlumatlar" onPress={() => navigation.navigate("Person")}>
          <MaterialCommunityIcons name={"account-outline"} size={30} color="white" />
        </ActionButton.Item>
        <ActionButton.Item buttonColor="#9b59b6" title="Axtarış" onPress={() => navigation.navigate("Filter")}>
          <MaterialCommunityIcons name={"file-search-outline"} size={30} color="white" />
        </ActionButton.Item>
        <ActionButton.Item buttonColor="#1abc9c" title="Yeni post" onPress={() => navigation.navigate("PostAdd")}>
          <MaterialCommunityIcons name={"circle-edit-outline"} size={30} color="white" />
        </ActionButton.Item>
        <ActionButton.Item buttonColor="#DA1212" title="Çıxış" onPress={exit}>
          <MaterialCommunityIcons name={"exit-to-app"} size={30} color="white" />
        </ActionButton.Item>
      </ActionButton>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    elevation: 1,
    margin: 0,
    //width: "99%",
    height: 260,
    //  backgroundColor: "#37a000",
    borderRadius: 3,
    padding: 10,
  },
  header: {
    fontWeight: "bold",
    fontSize: 17,
    color: "#1d4354",
    margin: 10,
  },
  footer: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: "#800000",
    borderRadius: 4,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  btnText: {
    color: "white",
    fontSize: 15,
    textAlign: "center",
  },
});
