import React, { useState, useEffect } from "react";
import { Text, StyleSheet, View, ScrollView, TouchableOpacity, ActivityIndicator } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { InputItem } from "./../../components/InputItem/Index";
import * as userService from "./../../services/UserService";
import Toast from "react-native-toast-message";

export default function Index() {
  const navigation = useNavigation();
  //   const { state, setState } = useMainContext();
  const [register, setRegister] = useState();
  const [loading, setLoading] = useState(false);
  const [repeatPassword, setRepeatPassword] = useState();

  const onChange = (name, val) => {
    setRegister({ ...register, [name]: val });
  };

  const onChangeRepeatPassword = (name, val) => {
    setRepeatPassword(val);
  };

  const registerUser = async () => {
    setLoading(true);
    if (register?.password === repeatPassword) {
      let result = await userService.addUser(register);
      if (result?.success) {
        Toast.show({ type: "success", text1: "Uğurlu əməliyyat", text2: "Sistemə qeydiyyatınız tamamlandı" });
        navigation.navigate("Login");
      } else {
        Toast.show({ type: "error", text1: "Uğursuz əməliyyat", text2: "Zəhmət olmasa məlumatları düzgün daxil edin" });
      }
    } else {
      Toast.show({ type: "error", text1: "Uğursuz əməliyyat", text2: "Şifrə və təkrar şifrə eyni deyil" });
    }
    setLoading(false);
  };

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}>
      <View style={{ marginLeft: 20, marginRight: 20 }}>
        <Text style={styles.headText}>Müəllimlərin qarşılıqlı yerdəyişməsi</Text>
        <InputItem icon={"account-outline"} placeholder={"Ad"} value={register?.email} name={"name"} onChange={onChange} />
        <InputItem icon={"account-outline"} placeholder={"Soyad"} value={register?.email} name={"surname"} onChange={onChange} />
        <InputItem icon={"email-outline"} placeholder={"Email adresi"} value={register?.email} name={"mail"} type={"email-address"} onChange={onChange} />
        <InputItem icon={"phone-outline"} placeholder={"Telefon nömrəsi"} value={register?.email} name={"phone"} type={"numeric"} onChange={onChange} />
        <InputItem icon={"lock-outline"} placeholder={"Şifrə"} value={register?.password} name={"password"} isSecure={true} onChange={onChange} />
        <InputItem icon={"lock-outline"} placeholder={"Şifrə (təkrar)"} value={repeatPassword} name={"repeatPassword"} isSecure={true} onChange={onChangeRepeatPassword} />
        <TouchableOpacity style={styles.login} onPress={registerUser}>
          {loading ? <ActivityIndicator size="large" color="white" /> : <Text style={{ color: "white", fontSize: 18, fontWeight: "bold" }}>Qeydiyyatdan keç</Text>}
        </TouchableOpacity>
        <View style={{ flexDirection: "row", justifyContent: "center", padding: 20 }}>
          <Text style={{ color: "#1d4354" }}>Artıq hesabınız var?</Text>
          <TouchableOpacity onPress={() => navigation.navigate("Login")}>
            <Text style={{ color: "#37a000" }}> Daxil ol</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  login: {
    height: 60,
    marginTop: 10,
    margin: 3,
    backgroundColor: "#37a000",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
  },
  loginText: { fontSize: 20, alignSelf: "center", color: "white" },
  headText: {
    justifyContent: "center",
    textAlign: "center",
    paddingBottom: 20,
    fontSize: 30,
    fontWeight: "bold",
  },
});
