import { get, post, put, deleteData } from "./core/Requests";

export const addUser = async (data) => {
  return await post(`/User/addUser`, data);
};

export const loginUser = async (data) => {
  return await post(`/User/loginUser`, data);
};

export const getUser = async () => {
  return await get(`/User/getUser`);
};

export const updateUser = async (data) => {
  return await post(`/User/updateUser`, data);
};

export const changePassword = async (data) => {
  return await post(`/User/changePassword`, data);
};