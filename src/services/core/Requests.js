import axios from "axios";
import { URL } from "./Urls";
import * as Storage from "./../../utils/Storage";

// const TOKEN = "Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9..497sBgkanufnMRXNve5gIR7cmyaZ2mL6AspKU7jX--j6x3su2u3II_v_WrfAxhLGZOtp0vAASk139Eh1m7bJEA";
async function headerAddToken(headers) {
  const TOKEN = await Storage.getItem("@token");
  headers != null ? (headers.Authorization = "Bearer " + TOKEN) : (headers = { Authorization: "Bearer " + TOKEN });
  return headers;
}

async function handleResponse(response) {
  //console.log(response);
  // return await response.data;

  return {
    data: await response.data,
    success: true,
    message: "Ugurlu",
  };
}

async function handleError(error) {
   console.log(error);
  //Alerts.error("error");
  let message = "";
  //   swal({
  //     title: "OOPS!",
  //     text: error,
  //     buttonsStyling: false,
  //     confirmButtonClass: "btn btn-error",
  //     type: "error",
  //   }).catch(swal.noop);
  if (error.response) {
    message = error.response.data.title;
    //  message.error({ content: error.response.data.Message });
    // Request made and server responded
  } else if (error.request) {
    message = error.request;
    // console.log(error.request);
    // The request was made but no response was received
    //  message.error({ content: error.request });
  } else {
    message = error.message;
    // console.log(error.message);
    // Something happened in setting up the request that triggered an Error
    // message.error({ content: error.message });
  }

  return {
    data: null,
    success: false,
    message: message,
  };
}

export async function get(endpoint, headData) {
  let headers =await headerAddToken(headData);
  return await axios
    .get(URL.BaseURL + endpoint, { headers })
    .then(handleResponse)
    .catch(handleError);
}

export async function post(endpoint, data, headData) {
  let headers =await headerAddToken(headData);
  return await axios
    .post(URL.BaseURL + endpoint, data, { headers })
    .then(handleResponse)
    .catch(handleError);
}

export async function put(endpoint, data, headData) {
  let headers =await headerAddToken(headData);
  return await axios
    .put(URL.BaseURL + endpoint, data, { headers })
    .then(handleResponse)
    .catch(handleError);
}

export async function deleteData(endpoint, headData) {
  let headers =await headerAddToken(headData);
  return await axios
    .delete(URL.BaseURL + endpoint, { headers })
    .then(handleResponse)
    .catch(handleError);
}

/*

          *********************          with toast         **************************

*/
