import AsyncStorage from '@react-native-async-storage/async-storage';

export async function getItem(key) {
  try {
    return await AsyncStorage.getItem(key);
  } catch (error) {
    return null;
  }
}

export async function setItem(key, value) {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    console.log(error);
  }
}

export async function removeItem(key, value) {
  try {
    await AsyncStorage.removeItem(key, value);
  } catch (error) {
    console.log(error);
  }
}
